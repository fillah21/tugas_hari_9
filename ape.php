<?php 
    require_once("animal.php");

    class Ape extends Animal {
        public $legs = 2;
        public $yell = "Auooo";

        public function yell() {
            echo "Name : " . $this->nama . "<br>"; 
            echo "Legs : " . $this->legs . "<br>"; 
            echo "Cold Blooded : " . $this->cold_blooded . "<br>"; 
            echo "Yell : " . $this->yell; // "Auooo"
        }
    }
?>