<?php 
    require_once("animal.php");
    require_once("frog.php");
    require_once("ape.php");

    $sheep = new Animal("shaun");

    echo "Name : " . $sheep->nama . "<br>"; // "shaun"
    echo "Legs : " . $sheep->legs . "<br>"; // 4
    echo "Cold Blooded : " . $sheep->cold_blooded . "<br>"; // "no"
    echo "<br>";

    $kodok = new Frog("buduk");
    $kodok->jump() ; // "hop hop"

    $sungokong = new Ape("kera sakti");
    $sungokong->yell() // "Auooo"

?>