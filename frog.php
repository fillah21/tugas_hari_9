<?php 
    require_once("animal.php");

    class Frog extends Animal {
        public $jump = "Hop Hop";
        
        public function jump() {
            echo "Name : " . $this->nama . "<br>"; 
            echo "Legs : " . $this->legs . "<br>"; 
            echo "Cold Blooded : " . $this->cold_blooded . "<br>"; 
            echo "Jump : " . $this->jump . "<br>"; // "hop hop"
            echo "<br>";
        }
    }
?>